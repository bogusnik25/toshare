#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
void generator(const int rows, const int columns, char field[rows][columns]);
void down_possible(const int rows, const int columns, char field[rows][columns], int x, int y);
bool check(const int rows, const int columns, char field[rows][columns]);
int overall_check(int x,int y,const int rows, const int columns, char field[rows][columns]);
void game_field(const int rows, const int columns, char field[rows][columns]);
void ball_sort_puzzle();
