#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include "ballsortpuzzle.h"
#define F_WHITE "\033[37m"
#define BLACK "\033[40m"
#define RED "\033[41m"
#define GREEN "\033[42m"
#define BROWN "\033[43m"
#define BLUE "\033[44m"
#define MAGENTA "\033[45m"
#define CYAN "\033[46m"
#define GRAY "\033[47m"
#define RESET "\033[0m"
int overall_check(int x,int y,const int rows, const int columns, char field[rows][columns]);
void generator(const int rows, const int columns, char field[rows][columns]) {
     for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            field[i][j] = ' ';
        }
    }
    srand(time(NULL));
    int x = 0;
    int y = 0; 
    char symbols[] = {'@', '#', '*', '^', '&', '%','-','+','$'};
    for (int i = 0; i < (columns - 2); i++) {
        for (int j = 0; j < rows; j++) {
            y = rand() % rows;
            x = rand() % (columns - 2);
            if (field[y][x] == ' ') {
                field[y][x] = symbols[i];
            } else {
                while (field[y][x] != ' ') {
                    y++;
                    if (y == rows) {
                        y = 0;
                        x++;
                        if (x == columns - 2) {
                            x = 0;
                        }
                    }
                }
                field[y][x] = symbols[i];
            }
        }
    }
    x = rand() % columns;
    if (field[0][x] != ' ') {
        for (int i = 0; i < rows; i++) {
            field[i][columns - 1] = field[i][x];
            field[i][x] = ' ';
        }
    }
    x = rand() % columns;
    if (field[0][x] != ' ') {
        for (int i = 0; i < rows; i++) {
            field[i][columns - 2] = field[i][x];
            field[i][x] = ' ';
        }
    }
}
void down_possible(const int rows, const int columns, char field[rows][columns], int x, int y) {
    if (overall_check(x,y,rows,columns,field) !=0) {
        return;
    }
    int character_to_move=0; //positon
    int character_to_be_moved=0; //position
    char element=' ';
    for (;character_to_move<rows;character_to_move++) {
        if (field[character_to_move][x-1]!=' ') {
            element=field[character_to_move][x-1];//holds the character to carry
            field[character_to_move][x-1]=' ';
            break;
        }
    }
    for (int j=rows;j>0;j--) {
        if (field[j][y-1]==' ') {
            character_to_be_moved=j;
            break;
        }
    }
   
        field[character_to_be_moved][y - 1] = element;
}
bool check(const int rows, const int columns, char field[rows][columns]) {
    for (int i=0;i<columns;i++) {
        for (int j = 0; j < rows; j++) {
            if ((field[j][i]!=field[0][i])) {
                return false;
            }
        }
    }
    return true;
}
void ball_sort_puzzle() {
    int rows = 0;
    int x = 0;
    int y = 0;
    int columns = 0;
    printf("Welcome to the game 'Ball sort puzzle'\n");
    printf("Type the desired field size: \n");
    scanf("%d %d", &rows, &columns);
  /* if (rows <=0 || columns>6 || columns<=0) {
        printf("Incorrect field size");
        return;
    }
    */
    char field[rows][columns];
    /*for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            field[i][j] = ' ';
        }
    }
    */
    generator(rows, columns, field);
    while (check(rows, columns, field) != 1) {
        game_field(rows,columns,field);
        printf("\n");
        printf("Type the position of the element to be moved and where to be moved: \n");
        scanf("%d %d", &x, &y);
        if (overall_check(x, y, rows, columns, field) == 1) {
            printf("Numbers are out of range\n");
            do {
                printf("Type the position of the element to be moved and where to be moved: \n");
                scanf("%d %d", &x, &y);
            } while (overall_check(x, y, rows, columns, field) != 0);
        }
        if (overall_check(x, y, rows, columns, field) == 2) {
            printf("The elements must be the same!\n");
            do {
                printf("Choose the elements which are the similar: \n");
                scanf("%d %d", &x, &y);
            } while (overall_check(x, y, rows, columns, field) != 0);
        }
        if (overall_check(x, y, rows, columns, field) == 3) {
            printf("The column is full\n");
            do {
                printf("Choose the column with space: \n");
                scanf("%d %d", &x, &y);
            } while (overall_check(x, y, rows, columns, field) != 0);
        }
    down_possible(rows, columns, field, x, y);
}
    printf("Congrats the game is won!");
    game_field(rows,columns,field);
}
int overall_check(int x,int y,const int rows, const int columns, char field[rows][columns]) {
    if ((x>columns || y<=0 )|| (x<=0 || y>columns)) {
        return 1;
    }
    int i=0;
    int j=0;
    for (;i<rows;i++) {
        if (field[i][x-1] != ' ') {
            break;
        }
    }
    for (;j<rows;j++) {
        if (field[j][y-1] != ' ') {
            break;
        }
    }
    if (j == rows) {
        return 0;
    }
    int size=0;
    for (int k=0;k<rows;k++) {
        if (field[k][y-1] != ' ') {
            size++;
        }
    }
    if (size==rows) {
        return 3;
    }
    if (field[i][x-1] != field[j][y-1]) {
        return 2;
    }
    return 0;
}
void game_field(const int rows, const int columns, char field[rows][columns]) {
    printf("\n");
    for (int i = 0; i < rows; i++) {
        printf("%d ", i + 1);
        for (int j = 0; j < columns; j++) {
            printf(" %s ", BLACK);
            switch (field[i][j]) {
                case '@': printf("%s", RED);
                break;
                case '*': printf("%s", GREEN);
                break;
                case '#': printf("%s", BROWN);
                break;
                case '^': printf("%s", BLUE); //{'@', '#', '*', '^', '&', '%','+'}
                break;
                case '%': printf("%s", MAGENTA);
                break;
                case '&': printf("%s", CYAN);
                break;
                default: printf("%s", GRAY);
            }
            printf(" %c %s %s", field[i][j], BLACK, RESET);
        }
        printf("\n");
    }
    printf("  ");
    for (int i = 0; i < columns; i++) {
        printf(" %s%s  %d  %s", F_WHITE, BLACK, i + 1, RESET);
    }
    printf("\n");
}
