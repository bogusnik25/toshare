#define _POSIX_C_SOURCE 200201L
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <curses.h>
#include <unistd.h>

// OK
void read_from_file(int all_figures[7][16]);
void init_field(int rows,int cols,int field[][cols]);
void to_figure(int future[4][4],int all_figures[7][16]);
void copying(int future[4][4],int figure_array[4][4]);
void write_to_field(int rows, int figure_array[4][4], int y, int columns, int cols, int field[rows][cols]);
void sleeping();
bool is_game_over(int columns, int y, int rows, int cols, int field[rows][cols], int figure_array[4][4]);
int printing_scores(int static_var_y,int static_var,int level,int counter,int cols,int scores);
void random_filling(int cols,int rows,int field[rows][cols],int level);
int last_layer(int figure_array[4][4],int columns,int cols,int rows,int input,int y,int field[rows][cols]);
int looking_for_line(int rows,int cols,int field[rows][cols]);
int length(int,char all[9][300]);
void clear_area();
void print_rules();

// UPDATED
void print_base(int rows,int cols,int static_var_y,int static_var);
void print_array_field(int static_var_y,int rows,int cols, int field[rows][cols], int static_var);
void paint_field(int static_var_y,int static_var,int y,int flag,int columns,int figure_array[4][4]);
int pretty_paint(int figure_array[4][4]); // delete ,int y

// NEW
bool check_rotating(int rows, int cols, int field[rows][cols], int y, int columns, int figure_array[4][4]);
void rotate(int figure_array[4][4]);
int fix_columns(int cols, int columns, int figure_array[4][4]);
int fix_y(int rows, int y, int figure_array[4][4]);

// DELETED
//void move_clean(int,int,int columns,int input,int figure_array[4][4],int c,int cols,int field[][cols]);
//int rotating_in_game(int figure_array[4][4],int columns,int cols,int y,int rows,int field[rows][cols]);
//int fix_position(int indicator,int columns,int y,bool flag);



//////////////////////////////////////////////////

void prints_next(int scores,int level,int rows,int static_var_y,int static_var,int future[][4],int columns,int cols);

//////////////////////////////////////////////////

void prints_next(int scores,int level,int rows,int static_var_y,int static_var,int future[][4],int columns,int cols) {
//    FILE *fp = fopen("best.txt","r+");
//    if (fp == NULL) exit(EXIT_FAILURE);
//    char best[4] = "";
//    while (fscanf(fp,"%s",best)!=EOF) {
//        fscanf(fp,"%s",best);
//    }
    int color = rand() % 6 + 2;
    attron(COLOR_PAIR(color));
    mvprintw(static_var_y+rows/2-5,static_var+cols*2+6,"NEXT FIGURE");
    attroff(COLOR_PAIR(color));
    for (int i = 0;i<4;i++) {
        for (int j = 0;j<4;j++) {
            attron(COLOR_PAIR(future[i][j]));
            mvprintw(static_var_y+rows/2+i-3,static_var+(cols+j)*2+7 ,"  ");
            attroff(COLOR_PAIR(future[i][j]));
        }
    }
    attron(COLOR_PAIR(3));
    mvprintw(static_var_y+rows/2+4,static_var+cols*2+9,"LEVEL");
    attroff(COLOR_PAIR(5));
    mvprintw(static_var_y+rows/2+5,static_var+cols*2+11,"%d",level);
    attron(COLOR_PAIR(3));
    mvprintw(static_var_y+rows/2+9,static_var+cols*2+9,"SPEED");
    attroff(COLOR_PAIR(3));
    switch(level) {
        case 1: {
            mvprintw(static_var_y+rows/2+10,static_var+cols*2+10,"EASY");
            break;
        }
        case 2: {
            mvprintw(static_var_y+rows/2+10,static_var+cols*2+9,"MEDIUM");
            break;
        }
        case 3: {
            if (scores >=1000) {
                mvprintw(static_var_y+rows/2+10,static_var+cols*2+9,"MASTER");
            }
            else {mvprintw(static_var_y+rows/2+10,static_var+cols*2+10,"HARD");
            }
            break;
        }
    }
    mvprintw(static_var_y,static_var-19,"Movement");
    mvprintw(static_var_y+2,static_var-26,"KEY_DOWN: move faster");
    mvprintw(static_var_y+3,static_var-26,"KEY_LEFT: move to left");
    mvprintw(static_var_y+4,static_var-26,"KEY_RIGHT: move to right");
    mvprintw(static_var_y+5,static_var-26,"SPACE: rotate the figure");
    mvprintw(static_var_y+6,static_var-26,"p,P: to pause the game");
    mvprintw(static_var_y+7,static_var-26,"ANY: to resume the game");
    mvprintw(static_var_y+8,static_var-26,"q,Q: to quit the game");
    mvprintw(static_var_y+rows/2+1,static_var - 20,"FIELD SIZE");
    mvprintw(static_var_y+rows/2+3,static_var - 17,"%dx%d",rows,cols);
//    mvprintw(static_var_y+rows/2+5,static_var - 20,"BEST SCORE");
//    mvprintw(static_var_y+rows/2+6,static_var - 16,"%s",best);
    mvprintw(static_var_y+rows/2+7,static_var - 20,"WORLD RECORD");
    mvprintw(static_var_y+rows/2+9,static_var - 19,"1 357 428");
//    fclose(fp);/
}

//////////////////////////////////////////////////



// OK
void read_from_file(int all_figures[7][16]) {
    FILE *fp = fopen("figures.txt", "r");
    if (fp == NULL) exit(EXIT_FAILURE);
    for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 16; j++) {
            fscanf(fp, "%1d", &all_figures[i][j]); //solved
        }
    }
    fclose(fp);
}

void init_field(int rows,int cols,int field[][cols]) {
    for (int i=0;i<rows;i++) {
        for (int j=0;j<cols;j++) {
            field[i][j] =  0;
        }
    }
}

void to_figure(int future[4][4], int all_figures[7][16]) {
    int tmp[4][4];
    int figure = rand() %  7; //which
    int number_of_rotations = rand() % 4; //position
    int color = rand() % 6 + 2;    //color
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            future[i][j] = (all_figures[figure][4 * i + j] == 1 ? color : 0); //now the best solution
        }
    }
    for (int c=0;c<number_of_rotations;c++) {
        for (int i=0;i<4;i++) {
            for (int j=0;j<4;j++) {
                tmp[j][3-i] = future[i][j];
            }
        }
        for (int i=0;i<4;i++) {
            for (int j=0;j<4;j++) {
                future[i][j] = tmp[i][j];
            }
        }
    }
}

void copying(int future[4][4],int figure_array[4][4]) {
    for (int i = 0;i<4;i++) {
        for (int j = 0;j<4;j++) {
            figure_array[i][j] = future[i][j];
        }
    }
}

void write_to_field(int rows, int figure_array[4][4], int y, int columns, int cols, int field[rows][cols]) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            if (figure_array[i][j] > 0) field[i + y][columns + j] = figure_array[i][j];
        }
    }
}

void sleeping() {
    struct timespec ts = {
            .tv_sec = 0,
            .tv_nsec = 100000000L
    };
    nanosleep(&ts,NULL);
}

bool is_game_over(int columns, int y, int rows, int cols, int field[rows][cols], int figure_array[4][4]) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            if (figure_array[i][j] > 0) {
                if (field[y + i][columns + j] > 0)return false;
            }
        }
    }
    return true;
}

int printing_scores(int static_var_y,int static_var,int level,int counter,int cols,int scores) {
    switch(counter) {
        case 1: {
            scores+=100 * level;
            break;
        }
        case 2: {
            scores+=300 * level;
            break;
        }
        case 3: {
            scores+=500 * level;
            break;
        }
        case 4: {
            scores+=800 * level;
            break;
        }
    }
    mvprintw(static_var_y+2,static_var+cols*2+15,"%d",scores);
    return scores;
}

void random_filling(int cols,int rows,int field[][cols],int level) {
    int indicator,col,indicator1,color;
    switch(level) {
        case 1: {
            indicator1 = 2;
            indicator = cols - 4;
            break;
        }
        case 2: {
            indicator1 = rows/5;
            indicator = (cols*2)/3;
            break;
        }
        case 3: {
            indicator1 = rows/3;
            indicator = cols/3;
            break;
        }
        default : {
            indicator1 = indicator = 0;
            break;
        }
    }
    for (int i = 0;i<indicator1;i++) {
        color = rand() % 6 + 2;
        for (int j = 0;j<indicator;j++) {
            col = rand() % cols;
            while (field[rows-1-i][col] > 0) {
                col++;
                if (col == cols) col=0;
            }
            field[rows-1-i][col] = color;
        }
    }
}

int last_layer(int figure_array[4][4],int columns,int cols,int rows,int input,int y,int field[rows][cols]) {
    if (input == 39) {
        for (int i = 0;i<4;i++) {
            for (int j=0;j<4;j++) {
                if (figure_array[i][j] > 0) {
                    if ((columns + 1 + j  == cols) || field[y+i][columns + j + 1] > 0) return 0;
                }
            }
        }
        return 1;
    }
    else if (input == 37) {
        for (int i=0;i<4;i++) {
            for (int j=0;j<4;j++) {
                if (figure_array[i][j] > 0) {
                    if ((columns - 1 + j < 0) || field[y+i][columns+j -1 ] > 0) return 0;
                }
            }
        }
        return 1;
    }
    else if (input == 0){
        for (int i=0;i<4;i++) {
            for (int j=0;j<4;j++) {
                if (figure_array[i][j] > 0) {
                    if (field[y+i+1][columns+j] > 0 || (y + i + 1) >= rows){
                        return 2;
                    }
                }

            }
        }
    }
    return 1;
}

int looking_for_line(int rows, int cols, int field[][cols]) {
    int j;
    int counter = 0;
    for (int i = 0; i < rows; i++) {
        j = 0;
        for (; j < cols; j++) {
            if (field[i][j] == 0) break;
        }
        if (j == cols) {
            counter++;
            for (int k = i; k > 0; k--) {
                for (int l = 0; l < cols; l++) {
                    field[k][l] = field[k - 1][l];
                }
            }
            for (int l = 0; l < cols; l++) {
                field[0][l] = 0;
            }
        }
    }
    return counter;
}

int length(int position,char all[9][300]) {
    int counter = 0;
    for (int j = 0;j<300;j++) {
        if (all[position][j] == '\0') break;
        counter++;
    }
    return counter;
}

void clear_area() {
    for (int i = 3;i<LINES-3;i++) {
        for (int j = 0;j<COLS;j++) {
            mvprintw(i,j," ");
        }
    }
}

void print_rules() {
    move(LINES/2-4,0);
    printw("Tetris has very simple rules: you can only move the pieces in specific ways;\n"
           "your game is over if your pieces reach the top of the screen; and you can\n"
           "only remove pieces from the screen by filling all the blank space in a line.\n"
           "Rules give much needed structure to this play. A completely random\n"
           "environment offers you no clue as to how to play and would be incredibly\n"
           "frustrating for u. How fortunate it is, then, that three rules are what\n"
           "shape it into such an award-winning game.Good luck!.");
    mvprintw(LINES-4,COLS-15,"PRESS q TO QUIT");
}

// UPDATED
void print_base(int rows, int cols, int static_var_y, int static_var) {
    mvprintw(static_var_y - 1, static_var - 1, "+");
    mvprintw(static_var_y - 1, static_var + cols * 2, "+");
    mvprintw(static_var_y + rows, static_var - 1, "+");
    mvprintw(static_var_y + rows, static_var + cols * 2, "+");
    move(static_var_y - 1, static_var);
    for (int i = 0; i < cols; i++) {
        printw("--");
    }
    move(static_var_y + rows, static_var);
    for (int i = 0; i < cols; i++) {
        printw("--");
    }
    for (int i = 0; i < rows; i++) {
        mvprintw(static_var_y + i, static_var - 1, "|");
        mvprintw(static_var_y + i, static_var + cols * 2, "|");
    }
    refresh();
}

void print_array_field(int static_var_y, int rows, int cols, int field[rows][cols], int static_var) {
    int color;
    for (int i = 0; i < rows; i++) {
        move(i + static_var_y, static_var);
        for (int j = 0; j < cols; j++) {
            color = (field[i][j] == 0 ? 8 : field[i][j]);
            attron(COLOR_PAIR(color));
            printw("  ");
            attroff(COLOR_PAIR(color));
        }
    }
}

void paint_field(int static_var_y,int static_var,int y,int flag,int columns,int figure_array[4][4]) {
    int color;
    if (flag == 0) {
        for (int i=0;i<4;i++) {
            for (int j = 0;j<4;j++) {
                if (figure_array[i][j] > 0) {
                    color = figure_array[i][j];
                    attron(COLOR_PAIR(color));
                    mvprintw(i + y + static_var_y, static_var + (columns + j) * 2, "  ");
                    attroff(COLOR_PAIR(color));
                }
            }
        }
    }
    else if (flag == 1) {
        for (int i = 0;i<4;i++) {
            for (int j = 0;j<4;j++) {
                if (figure_array[i][j] > 0){
                    color = 8;
                    attron(COLOR_PAIR(color));
                    mvprintw(static_var_y + i + y, static_var + (columns + j) * 2, "  ");
                    attroff(COLOR_PAIR(color));
                }
            }
        }
    }
}

int pretty_paint(int figure_array[4][4]) {
    int y = 0;
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j <= 4; j++) {
            if (j == 4) y--;
            else if (figure_array[i][j] > 0) return y;
        }
    }
    return y;
}

// NEW
bool check_rotating(int rows, int cols, int field[rows][cols], int y, int columns, int figure_array[4][4]) {
    int tmp[4][4];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            tmp[j][3 - i] = figure_array[i][j];
        }
    }
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; j++) {
            if (tmp[j][1 - i] > 0) {
                if (columns < i - 1) {
                    columns++;
                    break;
                }
            }
        }
    }
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; j++) {
            if (tmp[j][2 + i] > 0) {
                if (columns > cols - 3 - i) {
                    columns--;
                    break;
                }
            }
        }
    }
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; j++) {
            if (tmp[1 - i][j] > 0) {
                if (y < i - 1) {
                    y++;
                    break;
                }
            }
        }
    }
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; j++) {
            if (tmp[2 + i][j] > 0) {
                if (y > rows - 3 - i) {
                    y--;
                    break;
                }
            }
        }
    }
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            if (tmp[i][j] > 0) {
                if (field[y + i][columns + j] > 0) return false;
            }
        }
    }
    return  true;
}

void rotate(int figure_array[4][4]) {
    int tmp[4][4];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            tmp[j][3 - i] = figure_array[i][j];
        }
    }
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            figure_array[i][j] = tmp[i][j];
        }
    }
}

int fix_columns(int cols, int columns, int figure_array[4][4]) {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; j++) {
            if (figure_array[j][1 - i] > 0) {
                if (columns <  i - 1) {
                    columns++;
                    break;
                }
            }
        }
    }
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; j++) {
            if (figure_array[j][2 + i] > 0) {
                if (columns > cols - 3 - i) {
                    columns--;
                    break;
                }
            }
        }
    }
    return columns;
}

int fix_y(int rows, int y, int figure_array[4][4]) {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; j++) {
            if (figure_array[1 - i][j] > 0) {
                if (y <  i - 1) {
                    y++;
                    break;
                }
            }
        }
    }
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 4; j++) {
            if (figure_array[2 + i][j] > 0) {
                if (y > rows - 3 - i) {
                    y--;
                    break;
                }
            }
        }
    }
    return y;
}
