#include "header.h"

int main(int argc,char* argv[]) {
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    curs_set(FALSE);
    nodelay(stdscr, TRUE);
    start_color();
    init_pair(1, COLOR_WHITE, COLOR_BLACK); //out of range
    init_pair(2, COLOR_BLACK, COLOR_RED);
    init_pair(3, COLOR_BLACK, COLOR_GREEN);
    init_pair(4, COLOR_BLACK, COLOR_YELLOW);
    init_pair(5, COLOR_BLACK, COLOR_BLUE);
    init_pair(6, COLOR_BLACK, COLOR_MAGENTA);
    init_pair(7, COLOR_BLACK, COLOR_CYAN);
    init_pair(8, COLOR_BLACK, COLOR_WHITE);
    srand(time(NULL));

    int cols = 0;
    int rows = 0;
    int level = 0;

    bool flag = true;

    if (flag == true) {
        int indicator = 0;
        int menu_row = 0;
        int size;
        int counter = 0;
        int page = 0;
        int element = 0;
        bool flag_animation = true;
        bool flag_input = false;
        int position = 0;
        struct timespec ts = {
                .tv_sec = 0,
                .tv_nsec = 1.5*100000000L,
        };
        int menu_length = 0;
        char all[9][300] = {"START","RULES","LEAVE","EASY","MEDIUM","HARD","23x16","24x16","25x15"};

        while (true) {
            if (page == 3) {
                clear();
                refresh();
                break;
            }
            for (int m = 0;m<COLS;m++) {
                mvprintw(0,m,"=");
                mvprintw(2,m,"=");
                mvprintw(LINES-3,m,"=");
                mvprintw(LINES-1,m,"=");
            }
            refresh();
            mvprintw(1,counter," TETRIS");
            mvprintw(LINES-2,COLS-6-counter,"TETRIS ");
            counter++;
            if ((counter == COLS-5) ||(COLS-6-counter<0)) counter = 0;//
            if (counter <= 6) {
                mvprintw(1,COLS-5+counter," ");
                mvprintw(LINES-2,counter," ");
            }
            refresh();
            nanosleep(&ts,NULL);
            size = length(3 * page + element, all);
            if (flag_animation) {
                if (indicator < size) {
                    mvprintw((LINES- 3) / 2 + menu_row, (COLS - size) / 2 + indicator, "%c",all[3*page+element][indicator]);
                    indicator++;
                    mvprintw((LINES- 3) / 2 + menu_row, (COLS - size) / 2 + indicator, "_");
                    mvprintw((LINES- 3) / 2 + menu_row, (COLS - size) / 2 + size, " ");
                }
                else if (indicator == size){
                    menu_row++;
                    if (menu_row == 3)menu_row = 0;
                    indicator = 0;
                    element++;
                }
                if (element == 3) {
                    element = 0;
                    flag_animation = false;
                    flag_input = true;
                }
            }

            else if (flag_input == true){
                if (menu_length == 0) {
                    for (int j = 0;j<300;j++) {
                        if (all[3*page+position][j] == '\0')break;
                        menu_length++;
                    }
                }
                mvprintw((LINES - 3) / 2 + menu_row, (COLS - menu_length) / 2 - 2, ">");
                mvprintw((LINES - 3) / 2 + menu_row, (COLS + menu_length) / 2 + 1, "<");
                int input = getch();
                switch(input) {
                    case KEY_DOWN: {
                        mvprintw((LINES - 3) / 2 + menu_row, (COLS - menu_length) / 2 - 2, " ");
                        mvprintw((LINES - 3) / 2 + menu_row, (COLS + menu_length) / 2 + 1, " ");
                        menu_row++;
                        menu_length = 0;
                        position++;
                        if (menu_row == 3) menu_row = 0;
                        if (position == 3)position=0;
                        break;
                    }
                    case KEY_UP: {
                        mvprintw((LINES - 3) / 2 + menu_row, (COLS - menu_length ) / 2 - 2, " ");
                        mvprintw((LINES - 3) / 2 + menu_row, (COLS + menu_length) / 2 + 1, " ");
                        menu_row--;
                        menu_length = 0;
                        position--;
                        if (position<0)position = 2;
                        if (menu_row < 0)menu_row = 2;
                        break;
                    }
                    case 10: {
                        if (page == 0) {
                            if (menu_row == 0) {
                                clear_area();
                                refresh();
                                mvprintw(LINES/2-3,(COLS-4)/2,"LEVEL");
                                indicator = 0;
                                element = 0;
                                position = 0;
                                menu_length = 0;
                                flag_animation = true;
                                flag_input = false;
                                page++;
                                break;
                            }
                            else if (menu_row == 1){
                                clear_area();
                                print_rules();
                                refresh();
                                getchar();
                                clear_area();
                                indicator = 0;
                                menu_length = 0;
                                menu_row = 0;
                                flag_animation = true;
                                flag_input = false;
                            }
                            else if (menu_row == 2) {
                                endwin();
                                return EXIT_SUCCESS;
                            }
                        }
                        else if (page == 1 ) {
                            if (menu_row == 0) level = 1;
                            else if (menu_row == 1)level = 2;
                            else level = 3;
                            clear_area();
                            refresh();
                            mvprintw(LINES/2-3,(COLS-10)/2,"FIELD SIZE");
                            indicator = 0;
                            element = 0;
                            menu_length = 0;
                            menu_row = 0;
                            flag_animation = true;
                            flag_input = false;
                            page++;
                        }
                        else if (page == 2) {
                            if (menu_row == 0){
                                rows = 23;
                                cols = 16;
                            }
                            else if (menu_row == 1) {
                                rows = 24;
                                cols = 16;
                            }
                            else {
                                rows = 25;
                                cols = 15;
                            }
                            page++;
                        }
                        break;
                    }
                }
            }
        }
    }
    if (flag == false) {
        if (argc != 4) {
            printw("Error number of arguments shoud be at least 3 (rows cols level)");
            refresh();
            getchar();
            endwin();
            return (EXIT_FAILURE);
        }
    }

    int all_figures[7][16];
    int figure_array[4][4];
    int future[4][4];
    read_from_file(all_figures);

    if (flag == false) {
        sscanf(argv[3], "%d", &level);
        sscanf(argv[1], "%d", &rows);
        sscanf(argv[2], "%d", &cols);
        if (rows > 25 || cols > 17) {
            printw("The limit of optimal field size is exceeded");
            refresh();
            getchar();
            endwin();
            return EXIT_FAILURE;
        }
    }
    if (LINES < rows+2 || COLS < cols*2+17) {
        printw("The programm cannot be runned due to unsuitable terminal size LINES.min %d,COLS.min %d.Report a bug.", rows+2, cols*2+17);
        refresh();
        getchar();
        endwin();
        return EXIT_FAILURE;
    }

    int scores = 0;
    bool game_is_on = true;
    int input = 0; //user's key
    int field[rows][cols];
    init_field(rows, cols, field);
    to_figure(future, all_figures);
    random_filling(cols,rows,field,level);

    for (int i = 0;i<6;i++) {
        char string[] = "Scores";
        int color = i+2;
        attron(COLOR_PAIR(color));
        mvprintw((LINES-rows)/2+2,COLS/2+cols+8+i,"%c:",string[i]);//CHANGE!
        attroff(COLOR_PAIR(color));
        mvprintw((LINES-rows)/2+2,COLS/2+cols+15,"0");
    }
    refresh();

    while (game_is_on) {
        copying(future,figure_array); // writes from upper func
        to_figure(future, all_figures); //generates to the future purpose
        int static_var = COLS / 2 -cols;
        int static_var_y = (LINES - rows) / 2;/*pretty_paint(figure_array,y);*/ //pretty beginning of the figure
        int y = pretty_paint(figure_array); //offset of the figure
        int columns = cols / 2 - 2;
        int counter = 0;

        game_is_on = is_game_over(columns,y,rows,cols,field,figure_array);
        if (game_is_on == false){
            print_array_field(static_var_y, rows, cols, field, static_var);
            paint_field(static_var_y, static_var, y, 0, columns, figure_array);
            break;
        }

        print_base(rows, cols, static_var_y, static_var);
        prints_next(scores,level,rows,static_var_y,static_var,future,columns,cols);
        refresh();

        while (last_layer(figure_array, columns, cols, rows, 0, y, field) == 1) {
            print_array_field(static_var_y, rows, cols, field, static_var); //paint array
            paint_field(static_var_y, static_var, y, 0, columns, figure_array); //paints figure
            refresh();
            for (int k = 0; k <(scores >= 1000 ? 15 - (3*level + 4):12 - 3*level); k++) {
                input = getch();
                switch (input) {
                    case KEY_LEFT : {
                        if (last_layer(figure_array, columns, cols, rows, 37, y, field) == 1) {
                            paint_field(static_var_y, static_var, y, 1, columns, figure_array);
                            columns--;
                            paint_field(static_var_y, static_var, y, 0, columns, figure_array);
                            refresh();
                        }
                        break;                    }
                    case KEY_RIGHT: {
                        if (last_layer(figure_array, columns, cols, rows, 39, y, field) == 1) {
                            paint_field(static_var_y, static_var, y, 1, columns, figure_array);
                            columns++;
                            paint_field(static_var_y, static_var, y, 0, columns, figure_array);
                            refresh();
                        }
                        break;
                    }
                    case ' ' : {
                        if (check_rotating(rows, cols, field, y, columns, figure_array)) {
                            paint_field(static_var_y, static_var, y, 1, columns, figure_array);
                            rotate(figure_array);
                            columns = fix_columns(cols, columns, figure_array);
                            y = fix_y(rows, y, figure_array);
                            paint_field(static_var_y, static_var, y, 0, columns, figure_array);
                            refresh();
                        }
                        break;
                    }
                    case KEY_DOWN: {
                        if (last_layer(figure_array, columns, cols, rows, 0, y, field) == 1) {
                            paint_field(static_var_y, static_var, y, 1, columns, figure_array);
                            y++;
                            paint_field(static_var_y, static_var, y, 0, columns, figure_array);
                            refresh();
                        }
                        break;
                    }
                    case 'p' : case 'P': {
                        attron(COLOR_PAIR(8));
             mvprintw(static_var_y+rows/2,(static_var + cols-3),"PAUSE...");
                        refresh();
                        getchar();
                        mvprintw(static_var_y + rows/2,(static_var + cols-3),"        ");
                        attroff(COLOR_PAIR(8));
                        refresh();
                        break;
                    }
                    case 'q' : case 'Q' : {
                        endwin();
                        return(EXIT_SUCCESS);
                    }
                }
                sleeping();
            }
            if (last_layer(figure_array, columns, cols, rows, 0, y, field) == 1) {
                paint_field(static_var_y, static_var, y, 1, columns, figure_array);
                y++;
            }
        }
        write_to_field(rows, figure_array, y, columns, cols, field);
        counter = looking_for_line(rows,cols,field);
        scores = printing_scores(static_var_y,static_var,level,counter,cols,scores);
        refresh();
    }
    for (int i = 0;i<cols-2;i++) {
        mvprintw(LINES/2-1,COLS/2-cols+2+i*2," ");
        mvprintw(LINES/2,COLS/2-cols+2+i*2," ");
        mvprintw(LINES/2+1,COLS/2-cols+2+i*2," ");
    }
    mvprintw(LINES/2,COLS/2-cols+cols-5,"GAME OVER!");
    refresh();
    getchar();
    endwin();
    return EXIT_SUCCESS;
}
