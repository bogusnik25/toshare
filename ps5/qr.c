#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>
void encode_char(const char, bool bits[8]);
char decode_byte(const bool bits[8]);
void encode_string(const char string[], bool bytes[strlen(string)+1][8]);
void decode_bytes(const int rows, bool bytes[rows][8], char string[rows]);
void bytes_to_blocks(const int cols, const int offset, bool blocks[offset*8][cols], const int rows, bool bytes[rows][8]);
void blocks_to_bytes(const int cols, const int offset, bool blocks[offset*8][cols], const int rows, bool bytes[rows][8]);
int main() {
    return 0;
}
void encode_char(const char character, bool bits[8]) {
    int character_to_decimal=(int)character;
        if (character_to_decimal >= 128) {
            bits[0]=true;
            character_to_decimal=character_to_decimal-128;
        }
        else  {
            bits[0]=false;
        }
        if (character_to_decimal >= 64) {
            bits[1] = true;
            character_to_decimal=character_to_decimal - 64;
        }
        else {
            bits[1] = false;
        }
        if (character_to_decimal >=32) {
            bits[2] = true;
            character_to_decimal=character_to_decimal - 32;
        }
        else {
            bits[2] = false;
        }
        if (character_to_decimal >= 16 ) {
            bits[3] = true;
            character_to_decimal=character_to_decimal - 16;
        }
        else {
            bits[3] = false;
        }
        if (character_to_decimal >= 8 ) {
            bits[4] = true;
            character_to_decimal=character_to_decimal - 8;
        }
        else {
            bits[4] = false;
        }
        if (character_to_decimal >= 4) {
            bits[5] = true;
            character_to_decimal=character_to_decimal - 4;
        }
        else {
            bits[5] = false;
        }
        if (character_to_decimal >=2) {
            bits[6] = true;
            character_to_decimal=character_to_decimal - 2;
        }
        else {
            bits[6] = false;
        }
        if (character_to_decimal >= 1 ) {
            bits[7] = true;
            character_to_decimal=character_to_decimal - 1;
        }
        else {
            bits[7] = false;
        }
}
char decode_byte(const bool bits[8]) {
    int sum=0;
    int power=7;
    for (int i = 0; i < 8; i++) {
        if (bits[i] == 1) {
            sum+= pow(2,power);
        }
            power--;//6,5,4,3,2,1,0
        }
    return (char)sum;
}
void encode_string(const char string[], bool bytes[strlen(string)+1][8]) {
    int string_to_number = 0;
    int power=0;
    int size = strlen(string);
    for (int i = 0; i < size; i++) {
        power = 7;
        string_to_number = (int) string[i];
        for (int j = 0; j < 8; j++) {
            if (string_to_number >= pow(2,power)) {
                bytes[i][j] = true;
                string_to_number -= pow(2,power);
            }
            else {
                bytes[i][j] = false;
            }
            power--;
        }
    }
}
void decode_bytes(const int rows, bool bytes[rows][8], char string[rows]) {
    int sum;
    int power;
    for (int i = 0; i <= rows; ++i) {
        power = 7;
        sum=0;
        for (int j=0;j<8;j++) {
            if (bytes[i][j] == 1) {
                sum+=pow(2,power);
            }
            power--;
        }
        string[i]=(char)sum;
    }
}
void bytes_to_blocks(const int cols, const int offset, bool blocks[offset*8/*[16][3]*/][cols], const int rows, bool bytes[rows][8]) {
for (int i=0;i<rows;i++) {
    for (int j=0;j<8;j++) {
        blocks[(i/cols) * 8 + j][i%cols] = bytes[i][j]; // [] [1%3,2%3]
    }
}
}
void blocks_to_bytes(const int cols, const int offset, bool blocks[offset*8][cols], const int rows, bool bytes[rows][8]) {
    for (int i=0;i<rows;i++) {
        for (int j = 0; j < 8; ++j) {
            bytes[i][j] = blocks[(i/cols) *8 + j][i % cols];
        }
    }
}
