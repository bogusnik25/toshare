#include <stdio.h>
#include <math.h>
#include <stdlib.h>
float lift_a_car(const int,const int,const int);
float unit_price(const float,const int,const int);
int collatz(const int);
int opposite_number(const int,const int); 
void counter(const int input_array[],const int array_size,int result_array[]);
int array_max(const int input_array[],const int);
int array_min(const int input_array[],const int);
unsigned long sum_squared(const int);
unsigned long special_counter(const int input_array[],const int);
int special_numbers(const int input_array[], const int,int result_array[]);
int main() {
    return 0; 
}
float lift_a_car(const int stick_lenght,const int human_weight,const int car_weight) {
    float res=(float)stick_lenght*(float)human_weight / ((float)human_weight + (float)car_weight);
    float round_res= roundf(res*100)/100;
    return round_res;
}
float unit_price(const float pack_price,const int rolls_count,const int pieces_count) {
    float price_for_roll=pack_price/(float)rolls_count;
    float price_for_square=price_for_roll/(float)pieces_count;
    float price_for_100=price_for_square * (float)100;
    return round(price_for_100*100)/100;
}
int collatz(const int number) {
    int counter = 0;
    int saver = 0;
    if (number!=1) {
        if (number%2==0) {
            saver=number/2;
            counter+=1;
        }
        else {
            saver=(3*number)+1;
            counter+=1;
        }
    }
    else {
        saver=1;
    }
    for (;saver!=1;counter++) {
        if (saver%2==0){
            saver=saver/2;
        }
        else {
            saver=(saver*3)+1;
        }
    }
    counter+=1;
    return counter;


}
int opposite_number(const int n,const int number) {
        int distance=n/2;
        if(number>=distance) {
            return number - distance;
        }
            return number+distance;

}
void counter(const int input_array[], const int array_size, int result_array[2]) {
    int sum=0;//CONST
    //size=sizeof(input_array)/sizeof(input_array[0]);// CONST
    for (int i = 0; i < array_size; i+=2) { //for divisible by 2 positions
            sum+=input_array[i];
    }
    result_array[0]=sum;
    sum=0;
    for (int i = 1; i < array_size; i+=2) {
        sum+=input_array[i];
    }
    result_array[1]=sum;

}
unsigned long sum_squared(const int line) {
    unsigned long sum=0;
    int tmp[33];
    int array[33];
    for (int i=0;i<=line;i++) {
        for (int j=0;j<i;j++) {
            tmp[j]=array[j]; //position 0 = 1=tmp[j] 1 2
        }
        for (int j=0;j<=i;j++) {
            if (j==0 || j==i) {
                array[j]=1; // array[0]=1;
            }
            else {
                array[j]+=tmp[j-1];
            }
        }
    }
    for (int i=0;i<=line;i++) {
        sum+=(long)array[i]*(long)array[i];
    }
    return sum;
}

int array_max(const int input_array[], const int array_size) {
    int saver;
    if (input_array==NULL) {
        return -1;
    }
    if (array_size==1) {
        saver=input_array[0];
        return saver;
    }
    if (input_array[0]>input_array[1]) {
        saver=input_array[0];
    }
    else if(input_array[0]<input_array[1]) {
        saver = input_array[1];
    }
    for(int i=1;i<array_size-1;i++) {
        if(saver<input_array[i+1]) {
            saver=input_array[i+1];
        }
        else {
            continue;
        }
    }
    return saver;
}
int array_min(const int input_array[], const int array_size) {
    int saver;
    if (input_array==NULL) {
        return -1;
    }
    if (array_size==1) {
        saver=input_array[0];
        return saver;
    }
    if (input_array[0]>input_array[1]) {
        saver=input_array[1];
    }
    else if(input_array[0]<input_array[1]) {
        saver=input_array[0];
    }
    for(int i=1;i<array_size-1;i++) {
        if (saver > input_array[i + 1]) {
            saver = input_array[i + 1];
        }
    }
    return saver;
}
unsigned long special_counter(const int input_array[], const int array_size) {
    int sum=0;
    for (int i=0;i<array_size;i+=2) {
        sum=sum+input_array[i];
    }
    for (int i = 1; i <= array_size-1; i+=2) {
            sum=sum+(input_array[i]*input_array[i]);
    }
    return sum;
}
int special_numbers(const int input_array[], const int array_size,int result_array[]){
    int counter=0;
    int sum=0;
    int position=0;
    for (int i=0;i<array_size;i++) {
        counter=counter+1;
        position=i+1;
        sum=0;
        for (int j = 0; j < array_size-counter;j++) {
            sum+=input_array[position];
            position=position+1;
        }
        position=0;
        if (input_array[i]>sum) {
            result_array[i]=input_array[i];
        }
        else {
            result_array[i]=0;
        }
    }
    int tmp=0;
    for (int j=0;j<array_size-1;j++) {
        for (int i = 0; i <array_size-1; i++) {
            if (result_array[i] < result_array[i + 1]) {
                tmp = result_array[i + 1];
                result_array[i + 1] = result_array[i];
                result_array[i] = tmp;
            }
        }
    }
    int indicator=0;
    for (int i=0;i<array_size;i++) {
        if (result_array[i]!=0) {
            indicator++;
        }
    }
    return indicator;
}
