#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include "hangman.h"
bool already_used(char letters_guessed[],char available_letters[],const int i);
int word_is_guessed_updated(const char letters_guessed[],const char secret[],char guessed_word[]);
void function(char letters_guessed[],const int i,char guessed_word[],const char secret[],int attempts,char available_letters[],int letters_size);
int check(const char secret[],char letters_guessed[],char guessed_word[],const int i);
void print(char guessed_word[]);
void hangman(const char secret[]) {
    int secret_size = 0;
    for (int j = 0; j < strlen(secret); j++) {
        if (secret[j] != '\0') {
            secret_size++;
        }
    }
    int attempts = 8;
    char guessed_word[16]="";
    char letters_guessed[30]="";
    get_guessed_word(secret,letters_guessed,guessed_word);
    int letters_size=sizeof(letters_guessed);
    //int letters_guessed_size = strlen(letters_guessed);//changed from const for translating to lower letters
    char available_letters[27] = "abcdefghijklmnopqrstuvwxyz";
    printf("Welcome to the game, Hangman!\n");
    printf("I am thinking of a word that is %d letters long.\n", secret_size);
    printf("-------------\n");
    while (attempts!=0) {
    for (int i = 0; i < 16; i++) {
        printf("You have %d guesses left.\n", attempts);
        printf("Available letters: %s\n", available_letters);
        printf("Please guess a letter: ");
        fgets(&letters_guessed[i], letters_size, stdin);
        letters_guessed[strcspn(letters_guessed, "\n")] = 0;
        letters_guessed[i] = tolower(letters_guessed[i]);
        if (letters_guessed[i + 1] != '\0') {
            for (int j=0;j<strlen(letters_guessed);j++) {
                letters_guessed[j]=tolower(letters_guessed[j]);
            }
            if (word_is_guessed_updated(letters_guessed, secret, guessed_word) == 0) {
                printf("Congratulations, you won!\n");
                return ;
            } else {
                printf("The word hasn't been guessed");
                return;
            }
        }
        function(letters_guessed, i, guessed_word, secret, attempts, available_letters,letters_size);
        get_available_letters(letters_guessed,available_letters);
        if (check(secret,letters_guessed,guessed_word,i)==1) {
            attempts--;
        }
        printf("\n");
        printf("-------------\n");
        if (attempts==0) {
            break;
        }
            if (is_word_guessed(secret,letters_guessed)==1) {
                printf("Congratulations, you won!\n");
                return;
            }
    }
}
    printf("Sorry, you ran out of guesses. The word was %s.\n",secret);
}
void print(char guessed_word[]) {
    for (int i=0;i<strlen(guessed_word);i++) {
    if (i!=strlen(guessed_word)-1) {
        printf("%c ",guessed_word[i]);
    }
    else {
        printf("%c",guessed_word[i]);
    }
    }
}
int check(const char secret[],char letters_guessed[],char guessed_word[],const int i) {
        get_guessed_word(secret,letters_guessed,guessed_word);
        for (int j=0;j<strlen(secret);j++) {
            if (letters_guessed[i]==secret[j]) {
                printf("Good guess: ");
                print(guessed_word);
                return 0;
            }
        }
        printf("Oops! That letter is not in my word: ");
        print(guessed_word);
        return 1;
    }
void function(char letters_guessed[],const int i,char guessed_word[],const char secret[],int attempts,char available_letters[],int letters_size) {
    if (!isalpha(letters_guessed[i])) {
        do {
            get_guessed_word(secret, letters_guessed, guessed_word);
            printf("Oops! '%c' is not a valid letter: ", letters_guessed[i]);
            print(guessed_word);
             printf("\n");
            printf("-------------\n");
            printf("You have %d guesses left.\n",attempts);
            printf("Available letters: %s\n",available_letters);
            printf("Please guess a letter: ");
            fgets(&letters_guessed[i],letters_size,stdin);
            letters_guessed[i] = tolower(letters_guessed[i]);
            letters_guessed[strcspn(letters_guessed, "\n")] = 0;
        }
        while (!isalpha(letters_guessed[i]));
    }
    if (already_used(letters_guessed,available_letters,i)==0) {
        do {
            get_guessed_word(secret, letters_guessed, guessed_word);
            printf("Oops! You've already guessed that letter: ");
            print(guessed_word);
              printf("\n");
            printf("-------------\n");
            printf("You have %d guesses left.\n", attempts);
            printf("Available letters: %s\n", available_letters);
            printf("Please guess a letter: ");
            fgets(&letters_guessed[i], letters_size, stdin);
            letters_guessed[i] = tolower(letters_guessed[i]);
            letters_guessed[strcspn(letters_guessed, "\n")] = 0;
        }
        while (already_used(letters_guessed,available_letters,i)!=1);
        get_available_letters(letters_guessed,available_letters);
    }
}
/*void ask_again(char letters_guessed[],const int i,char available_letters[]) {
    do {
        printf("Now it is time to guess a letter.Type the letter in(the program will ask eternally if not a letter or letter has already been entered): ");
        *//*while (getchar() != '\n');*//*
        fgets(&letters_guessed[i], 9, stdin);
        letters_guessed[i] = tolower(letters_guessed[i]);
    }
    while (!already_used(letters_guessed, available_letters,i));
}*/
bool already_used(char letters_guessed[],char available_letters[],const int i) {
    for (int j=0;j< strlen(available_letters);j++) {
        if (letters_guessed[i]==available_letters[j]) {
            return 1;
        }
    }
    return 0;
}
int word_is_guessed_updated(const char letters_guessed[],const char secret[],char guessed_word[]) {
    get_guessed_word(secret, letters_guessed, guessed_word);
    return strcmp(secret, guessed_word);
}
void get_available_letters(const char letters_guessed[], char available_letters[]) { //returns unused letters
    char available_letters_copy[] = "abcdefghijklmnopqrstuvwxyz";//represents available_letters+size = size+1
    int letters_guessed_size = strlen(letters_guessed);
//    int available_letters_copy_size = strlen(available_letters_copy);
    for (int i = 0; i < letters_guessed_size; i++) {
        for (int j = 0; j < strlen(available_letters_copy); j++) {
            if (letters_guessed[i] == available_letters_copy[j]) {
                for (int k = j; available_letters_copy[k] != '\0'; ++k) {
                    available_letters_copy[k] = available_letters_copy[k + 1];
                } //bcdefghijklmnopqrstuvwxyz
                available_letters_copy[strlen(available_letters_copy)] = '\0';
            }
        }
    }
    strcpy(available_letters, available_letters_copy);
}

void get_guessed_word(const char secret[], const char letters_guessed[], char guessed_word[]) { //function builds the guessed word
    int letters_guessed_length = strlen(letters_guessed);
    int secret_length = strlen(secret);
    int indicator=0;
    while (indicator!=1) {
        for (int i=0;i<secret_length;i++) {
            for (int j=0;j<=letters_guessed_length;j++) {
                if (j==strlen(letters_guessed)) {
                    guessed_word[i]='_';
                }
            }
        }
        indicator++;
    }
  for (int i=0;i<letters_guessed_length;i++) {
      for (int j=0;j<secret_length;j++) {
          if (letters_guessed[i]==secret[j]) {
              guessed_word[j]=secret[j];
          }
      
  }
  }
  guessed_word[secret_length]='\0';
}


//function checks if from letters if possible a word to be completed
int is_word_guessed(const char secret[], const char letters_guessed[]) {
    int secret_len=strlen(secret);
    int letters_guessed_len= strlen(letters_guessed);
    int match=0;
    for (int i=0;i<secret_len;i++) {
        for (int j = 0; j < letters_guessed_len; j++) {
            if (secret[i]==letters_guessed[j]) {
                match += 1;
            }
        }
    }
    if (match==secret_len) {
        return 1;
    }
    return 0;
}

int get_word(char secret[]){
    // check if file exists first and is readable
    FILE *fp = fopen(WORDLIST_FILENAME, "rb");
    if( fp == NULL ){
        fprintf(stderr, "No such file or directory: %s\n", WORDLIST_FILENAME);
        return 1;
    }

    // get the filesize first
    struct stat st;
    stat(WORDLIST_FILENAME, &st);
    long int size = st.st_size;

    do{
        // generate random number between 0 and filesize
        long int random = (rand() % size) + 1;
        // seek to the random position of file
        fseek(fp, random, SEEK_SET);
        // get next word in row ;)
        int result = fscanf(fp, "%*s %20s", secret);
        if( result != EOF )
            break;
    }while(1);

    fclose(fp);

    return 0;
}
