#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include "hangman.h"
int main() {
    char secret[16];
    get_word(secret);
    hangman(secret);
    return 0;
}
